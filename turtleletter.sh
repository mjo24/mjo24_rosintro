#!/usr/bin/bash

rosservice call /reset
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0, 3.5, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[1.5, -3, 0.0]' '[0.0, 0.0, 0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
       	-- '[1.5, 3, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0, -3.5, 0.0]' '[0.0, 0.0, 0]'

