#!/usr/bin/bash

rosservice call /reset
rosservice call /turtle1/set_pen 0 0 0 0 1
rosservice call /turtle1/teleport_absolute 1.5 4 1.571	
rosservice call /turtle1/set_pen 128 0 128 4 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[3.5, 0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[-3, -1.5, 0.0]' '[0.0, 0.0, 0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[3, -1.5, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[-3.5, 0, 0.0]' '[0.0, 0.0, 0]'

rosservice call /turtle1/set_pen 0 0 0 0 1
rosservice call /turtle1/teleport_absolute 7.7 4 0
rosservice call /turtle1/set_pen 255 192 203 4 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[8, 0, 0.0]' '[0.0, 0.0, 4.5]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[8, 0, 0.0]' '[0.0, 0.0, 4.5]'
